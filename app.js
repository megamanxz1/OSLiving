// Slick Slider
$('.section-product__content__card').slick({
  infinite: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  prevArrow:"<button type='button' class='slick-prev section-product__content__header__button__pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
  nextArrow:"<button type='button' class='slick-next section-product__content__header__button__pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});

$('.section-gallery__card').slick({
  infinite: false,
  slidesToShow: 2,
  arrows: true,
  prevArrow:"<button type='button' class='slick-prev section-gallery__card__button-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
  nextArrow:"<button type='button' class='slick-next section-gallery__card__button-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 380,//đang 380 nè
      settings: {
        slidesToShow: 1,
      }
    }
  ]
  });
// Slick Slider

// Form validation
  //name
  function validateName() {
    let x = document.forms["myForm"]["name"].value;
    if (x == "") {
      alert("Làm ơn nhập tên của bạn");
      document.forms["myForm"]["name"].focus;
      return false;
    }
  }
  //phone
  function validatePhone() {
    let x = document.forms["myForm"]["phone"].value;
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    if (x == "") {
      alert("Hãy nhập số điện thoại của bạn");
      document.forms["myForm"]["phone"].focus;
      return false;
    } else if (vnf_regex.test(x) == false) {
      alert("Số điện thoại của bạn không đúng định dạng!");
      document.forms["myForm"]["phone"].focus;
      return false;
    }
  }
  //email
  function validateEmail() {
    let x = document.forms["myForm"]["email"].value;
    const filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (x == "") {
      alert("Làm ơn nhập email của bạn");
      document.forms["myForm"]["email"].focus;
      return false;
    } else if (!filter.test(x)) {
      alert("Làm ơn nhập đúng mẫu email");
      document.forms["myForm"]["email"].focus;
      return false;
    }
  }
  //project need 2 fix
  function validateProject() {
    let x = document.forms["myForm"]["project"].value;
    if (x == "") {
      alert("Làm ơn chọn dự án bạn muốn");
      document.forms["myForm"]["project"].focus;
      return false;
    }
  }
  //issue need 2 fix
  function validateIssue() {
    let x = document.forms["myForm"]["issue"].value;
    if (x == "") {
      alert("Làm ơn chọn vấn đề bạn quan tâm");
      document.forms["myForm"]["issue"].focus;
      return false;
    }
  }
  //main validate
  function validateForm() {
    validateName();
    validatePhone();
    validateEmail();
    validateProject();
    validateIssue();
  }
  // Form validation

//   document.getElementsByName("submit").addEventListener("click", function(event){
//   event.preventDefault()
// });